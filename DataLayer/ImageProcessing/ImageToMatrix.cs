﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.ImageProcessing
{
   public class ImageToMatrix
    {
        public static Color[][] GetBitMapColorMatrix(string bitmapFilePath)
        {
           
            Bitmap b1 = new Bitmap(bitmapFilePath);

            int hight = b1.Height;
            int width = b1.Width;

            Color[][] colorMatrix = new Color[width][];
            for (int i = 0; i < width; i++)
            {
                colorMatrix[i] = new Color[hight];
                for (int j = 0; j < hight; j++)
                {
                    colorMatrix[i][j] = b1.GetPixel(i, j);
                }
            }
            return colorMatrix;
        }
        public static byte[,] ByteConvertMatrix(byte[] barray)
        {
           
            byte[,] matrix = new byte[111,108];
            Buffer.BlockCopy(barray, 0, matrix, 0, barray.Length * sizeof(byte));
            return matrix;
        }
    }
}

