﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Drawing;
namespace DataLayer.ImageProcessing
{
    public class ImageToByte
    {

        public byte[] imageToByteArray(Image image)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                image.Save(memoryStream, image.RawFormat);
                return memoryStream.ToArray();
            }
        }
        public Image byteArrayToImage(byte[] imageByteArray)
        {
            using (MemoryStream ms = new MemoryStream(imageByteArray))
            {
                Image returnImage = Image.FromStream(ms);
                return returnImage;
            }
        }
    }
}
