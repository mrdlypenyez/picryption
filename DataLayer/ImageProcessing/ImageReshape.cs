﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace DataLayer.ImageProcessing
{
    public class ImageReshape
    {

        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            if (image.Width <= width)
            {
                width = image.Width;
            }
            if (image.Height <= height)
            {
                height = image.Height;
            }
            Bitmap bmpt = new Bitmap(width, height);
            Graphics grt = Graphics.FromImage(bmpt);
            grt.CompositingQuality = CompositingQuality.Default;
            grt.SmoothingMode = SmoothingMode.Default;
            grt.InterpolationMode = InterpolationMode.Bicubic;
            grt.PixelOffsetMode = PixelOffsetMode.Default;
            grt.DrawImage(image, 0, 0, width, height);
            return bmpt;
        }
    }
}
