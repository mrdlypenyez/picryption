﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.FileIO
{
    public class FileManager
    {
        public static string sadress;

        public string GetImageAdress()
        {
            string adress;
            
            OpenFileDialog open = new OpenFileDialog();
            // image filters  
            open.Filter = "Image Files(*.jpg; *.jpeg; *.bmp)|*.jpg; *.jpeg; *.bmp";
            Nullable<bool> result = open.ShowDialog();
            if (result == true)
            {
                // display image in picture box  
                // pictureBox1.Image = new Bitmap(open.FileName);
                // image file path  
                adress = open.FileName;
            }
            else
            {
                Console.Write("error file selected.");
                adress = "";
            }
            sadress = adress;
            return adress;
        }
    }
}
