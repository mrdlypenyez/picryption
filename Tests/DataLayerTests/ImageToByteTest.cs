﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using DataLayer.ImageProcessing;

namespace Tests.DataLayerTests
{
    /// <summary>
    /// ImageToByteTest için kısa açıklama
    /// </summary>
    [TestClass]
    public class ImageToByteTest
    {
        public ImageToByteTest()
        {
            //
            // TODO: Buraya oluşturucu mantığı ekleyin.
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Geçerli test çalışması hakkında bilgi ve bu testin işlevini
        ///sağlayan test bağlamını alır ya da ayarlar.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Ek test öznitelikleri
        //
        // Testlerinizi yazarken şu ek öznitelikleri kullanabilirsiniz:
        //
        // Sınıftaki ilk testi çalıştırmadan önce kodu çalıştırmak için ClassInitialize kullanın
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Sınıftaki tüm testler çalıştırıldıktan sonra kodu çalıştırmak için ClassCleanup kullanın
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Her bir testi çalıştırmadan önce kodu çalıştırmak için TestInitialize kullanın 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Her bir testi çalıştırdıktan sonra kodu çalıştırmak için TestCleanup kullanın
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void ImagetoByte_SimpleImageConvert_ReturnsByteArrayIsNull()
        {

            Image img = Image.FromFile(@"C:\Users\Zeynep\source\repos\picryption\images\prince.jpg");

            ImageToByte ib = new ImageToByte();
            byte[] bit = ib.imageToByteArray(img);

            //byte[] expectedBit = null;
            //Assert.AreNotEqual(expectedBit, bit);
            Assert.IsNotNull(bit);
        }

        [TestMethod]
        public void BytetoImage_SimpleByteConvert_ReturnsImageIsSame()
        {
            ImageToByte ib = new ImageToByte();
            Image img = Image.FromFile(@"C:\Users\Zeynep\source\repos\picryption\images\prince.jpg");

            byte[] bit = ib.imageToByteArray(img);
            Image exactedImage = ib.byteArrayToImage(bit);
            try
            {
                Assert.AreEqual(exactedImage, img);
            }catch(AssertFailedException ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            
        }
    }
}
