﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using DataLayer.ImageProcessing;
using System.Drawing;

namespace Tests.DataLayerTests
{
    /// <summary>
    /// ImagetoMatrixTest için kısa açıklama
    /// </summary>
    [TestClass]
    public class ImagetoMatrixTest
    {
        public ImagetoMatrixTest()
        {
            //
            // TODO: Buraya oluşturucu mantığı ekleyin.
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Geçerli test çalışması hakkında bilgi ve bu testin işlevini
        ///sağlayan test bağlamını alır ya da ayarlar.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Ek test öznitelikleri
        //
        // Testlerinizi yazarken şu ek öznitelikleri kullanabilirsiniz:
        //
        // Sınıftaki ilk testi çalıştırmadan önce kodu çalıştırmak için ClassInitialize kullanın
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Sınıftaki tüm testler çalıştırıldıktan sonra kodu çalıştırmak için ClassCleanup kullanın
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Her bir testi çalıştırmadan önce kodu çalıştırmak için TestInitialize kullanın 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Her bir testi çalıştırdıktan sonra kodu çalıştırmak için TestCleanup kullanın
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void GetBitMapColorMatrix_ImageConverter_ResultMatrixIsNull()
        {
            Color[][] returnMatrix;
            

            string path = @"C:\Users\Zeynep\source\repos\picryption\images\prince.jpg";
            returnMatrix = ImageToMatrix.GetBitMapColorMatrix(path);
            Assert.IsNotNull(returnMatrix);
        }
    }
}
