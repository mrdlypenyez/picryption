﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataLayer.ImageProcessing;
using DataLayer.FileIO;
using System.Drawing;
using System.Diagnostics;

namespace PresentationLayer
{
    /// <summary>
    /// MainWindow.xaml etkileşim mantığı
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        FileManager fileManager = new FileManager();
        BitmapImage x;
        string imageAdress;
        private void btn_get_file_Click(object sender, RoutedEventArgs e)
        {
            //string imageAdress = fileManager.GetImageAdress();
            imageAdress = fileManager.GetImageAdress();

            try
            {
             //   Image imageResized = Image.FromFile(imageAdress);
               x = new BitmapImage(new Uri(imageAdress));
               // imageResized.
                image.Source = x;


            }
            catch (Exception ex)
            {
                MessageBox.Show("picture could not be selected");
            }
        }

       /* private void get_byte_Btn_Click(object sender, RoutedEventArgs e)
        {
            System.Drawing.Image img = System.Drawing.Image.FromFile(imageAdress);
            ImageToByte ib = new ImageToByte();
            byte[] bit = ib.imageToByteArray(img);
            // Console.WriteLine(Encoding.Default.GetString(bit));
            Console.WriteLine(bit);
        }*/
    }
}
